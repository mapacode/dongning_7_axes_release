#!bin/sh
version=dl7axis_4.0.8.31.4

if [ ! -d var ]; then
   mkdir var
   mkdir var/run
   mkdir var/machining
   mkdir var/history
fi

if [ ! -d var/history ]; then
   mkdir var/history
fi

tar xzf update/$version.tar.gz -C update/

if [ -d update/$version ]; then

   tar cf backup.tar etc/ scripts/ var/ exe/ bin/ mapahmi/
   mv -f backup.tar downloads/

   mv -f update/$version/bin/* bin/
   chmod 755 bin/*
   
   mv -f update/$version/dl7.msl .

   rm -R mapahmi
   mv -f update/$version/mapahmi .
   cd mapahmi
   rm -f application.json
   ln -s application.json.$MECHINETYPE application.json  
   cd ..

   mv -f update/$version/etc/setupCNC.xml etc/ 
   mv -f update/$version/etc/CMSG1.txt etc/ 
   mv -f update/$version/scripts/edm-run.bash scripts/
   mv -f update/$version/scripts/shutdownHMI.expect scripts/

   cp -n update/$version/etc/machine_ref.json.origin etc/machine_ref.json
   cp -n update/$version/etc/relative_origin.json.origin etc/relative_origin.json
   cp -n update/$version/etc/workpiece_origin.json.origin etc/workpiece_origin.json
   cp -n update/$version/etc/tool_table.json.origin etc/tool_table.json

   if  grep -iqE 'INSTALLED' etc/parameters.xml  ; then 
      cp -f etc/parameters.xml downloads/parameters.xml.backup
      cd update/$version/parameters
      node upgrade.js edm ../../../etc/parameters.xml ../../../etc/parameters.xml
      cd ../../../
   fi  

   rm -rf update/$version
   rm update/$version.tar.gz

fi
